﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AcmeSoftWebPage.aspx.cs" Inherits="AcmeSoftApplication.AcmeSoftWebPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form runat="server" id="form1">
    <div runat="server" id="d">
<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="EmployeeId"
OnRowDataBound="OnRowDataBound" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" EmptyDataText="No records has been added.">
<Columns>
    <asp:TemplateField HeaderText="Last Name" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtLastName" runat="server" Text='<%# Eval("LastName") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField HeaderText="First Name" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtFirstName" runat="server" Text='<%# Eval("FirstName") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>
        <asp:TemplateField HeaderText="Birth Date" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblBirthDate" runat="server" Text='<%# Eval("BirthDate") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtBirthDate" runat="server" Text='<%# Eval("BirthDate") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>
            <asp:TemplateField HeaderText="Employee Number" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblEmployeeNum" runat="server" Text='<%# Eval("EmployeeNum") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtEmployeeNum" runat="server" Text='<%# Eval("EmployeeNum") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>
                <asp:TemplateField HeaderText="Employee Date" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblEmployeeDate" runat="server" Text='<%# Eval("EmployeeDate") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtEmployeeDate" runat="server" Text='<%# Eval("EmployeeDate") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Terminate Date" ItemStyle-Width="150">
        <ItemTemplate>
            <asp:Label ID="lblTerminatedate" runat="server" Text='<%# Eval("Terminatedate") %>'></asp:Label>
        </ItemTemplate>
        <EditItemTemplate>
            <asp:TextBox ID="txtTerminatedate" runat="server" Text='<%# Eval("Terminatedate") %>'></asp:TextBox>
        </EditItemTemplate>
    </asp:TemplateField>
    <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150"/>
</Columns>
</asp:GridView>
<table border="1" style="border-collapse: collapse">
<tr>
    <td style="width: 150px">
        LastName:<br />
        <asp:TextBox ID="txtLastName" runat="server" Width="140" />
    </td>
    <td style="width: 150px">
        First Name:<br />
        <asp:TextBox ID="txtFirstName" runat="server" Width="140" />
    </td>
        <td style="width: 150px">
        Birth Date:<br />
        <asp:TextBox ID="txtBirthDate" runat="server" Width="140" />
    </td>
    <td style="width: 150px">
        Employee Number:<br />
        <asp:TextBox ID="txtEmployeeNumber" runat="server" Width="140"/>
    </td>
    <td style="width: 150px">
        Employee Date:<br />
        <asp:TextBox ID="txtEmployeeDate" runat="server" Width="140"/>
    </td>
     <td style="width: 150px">
        Terminate Date:<br />
         <asp:TextBox ID="txtTerminatedate" runat="server" Width="140"/>
     </td>
    <td style="width: 150px">
        <asp:Button ID="btnAdd"  runat="server" Text="Add" OnClick="Insert" />
    </td>
</tr>
</table>
    </div>

</form>

</body>
</html>
