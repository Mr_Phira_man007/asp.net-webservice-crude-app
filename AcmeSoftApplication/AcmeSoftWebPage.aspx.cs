﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AcmeSoftApplication
{
    public partial class AcmeSoftWebPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.BindGrid();
            }
        }
        private void BindGrid()
        {
            AcmeSoftWebService acmeSoftWebService = new AcmeSoftWebService(); 
            GridView1.DataSource = acmeSoftWebService.Get();
            GridView1.DataBind();
        }

        protected void Insert(object sender, EventArgs e)
        {
            AcmeSoftWebService acmeSoftWebService = new AcmeSoftWebService();
            acmeSoftWebService.InsertEmployee(txtLastName.Text, txtFirstName.Text, txtBirthDate.Text, txtEmployeeNumber.Text, txtEmployeeDate.Text, txtTerminatedate.Text);
            this.BindGrid();
        }
        protected void OnRowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            this.BindGrid();
        }
        protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            string employeeId = Convert.ToString(GridView1.DataKeys[e.RowIndex].Values[0]);
            string lastName = (row.FindControl("txtLastName") as TextBox).Text;
            string firstName = (row.FindControl("txtFirstName") as TextBox).Text;
            string birthDate = (row.FindControl("txtBirthDate") as TextBox).Text;
            string employeeNum = (row.FindControl("txtEmployeeNum") as TextBox).Text;
            string employeeDate = (row.FindControl("txtEmployeeDate") as TextBox).Text;
            string terminateDate = (row.FindControl("txtTerminatedate") as TextBox).Text;
           

            AcmeSoftWebService acmeSoftWebService = new AcmeSoftWebService();
            acmeSoftWebService.UpdateEmployee(employeeId, lastName, firstName,birthDate,employeeNum,employeeDate,terminateDate);
            GridView1.EditIndex = -1;
            this.BindGrid();
        }
        protected void OnRowCancelingEdit(object sender, EventArgs e)
        {
            GridView1.EditIndex = -1;
            this.BindGrid();
        }
        protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string employeeId = Convert.ToString(GridView1.DataKeys[e.RowIndex].Values[0]);
            AcmeSoftWebService acmeSoftWebService = new AcmeSoftWebService();
            acmeSoftWebService.DeleteEmployee(employeeId);
            this.BindGrid();
        }
        
        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {

            //if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != GridView1.EditIndex)
            //{
            //     (e.Row.Cells[2].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('Do you want to delete this row?');";            
            //}
        }
        
    }
}