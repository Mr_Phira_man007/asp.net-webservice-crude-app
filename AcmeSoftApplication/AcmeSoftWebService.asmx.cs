﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace AcmeSoftApplication
{
    /// <summary>
    /// Summary description for AcmeSoftWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AcmeSoftWebService : System.Web.Services.WebService
    {
        private string ConnectionState()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString;
            return connectionString;
        }

        [WebMethod]
        public DataTable Get()
        {
            using (SqlConnection sqlConnection = new SqlConnection(ConnectionState()))
            {
                using (SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Employee, Person WHERE Employee.PersonId = Person.PersonId"))
                {
                    using (SqlDataAdapter sqldataAdapter = new SqlDataAdapter())
                    {
                        sqlCommand.Connection = sqlConnection;
                        sqldataAdapter.SelectCommand = sqlCommand;
                        using (DataTable dataTable = new DataTable())
                        {
                            dataTable.TableName = "Employee";
                            sqldataAdapter.Fill(dataTable);
                            return dataTable;
                        }
                    }
                }
            }
        }

        [WebMethod]
        public void InsertEmployee(string lastName, string firstName,string birthDate,string employeeNum,string employeeDate,string terminatedate)
        {
            int personId;
           
            using (SqlConnection sqlConnection = new SqlConnection(ConnectionState()))
            {
                using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO Person (LastName, FirstName, BirthDate) VALUES (@LastName, @FirstName, @BirthDate) SELECT SCOPE_IDENTITY()"))
                {
                    sqlCommand.Parameters.AddWithValue("@LastName", lastName);
                    sqlCommand.Parameters.AddWithValue("@FirstName", firstName);
                    sqlCommand.Parameters.AddWithValue("@BirthDate", birthDate);

                    sqlCommand.Connection = sqlConnection;
                    sqlConnection.Open();
                    personId = Convert.ToInt32(sqlCommand.ExecuteScalar());
                    sqlConnection.Close();
                }

                using (SqlCommand sqlCommand = new SqlCommand("INSERT INTO Employee (PersonId, EmployeeNum, EmployeeDate, Terminatedate) VALUES (@PersonId, @EmployeeNum, @EmployeeDate, @Terminatedate)"))
                {
                    sqlCommand.Parameters.AddWithValue("@PersonId", personId.ToString());
                    sqlCommand.Parameters.AddWithValue("@EmployeeNum", employeeNum);
                    sqlCommand.Parameters.AddWithValue("@EmployeeDate", employeeDate);
                    sqlCommand.Parameters.AddWithValue("@Terminatedate", terminatedate);

                    sqlCommand.Connection = sqlConnection;
                    sqlConnection.Open();
                    sqlCommand.ExecuteNonQuery();
                    sqlConnection.Close();
                }
            }
        }

        [WebMethod]
        public void UpdateEmployee(string employeeId, string lastName, string firstName, string birthDate, string employeeNum, string employeeDate, string terminatedate)
        {
            string personId;
            using (SqlConnection sqlConnection = new SqlConnection(ConnectionState()))
            {
                string sql = "SELECT PersonId FROM Employee WHERE Employee.EmployeeId = @EmployeeId";
                SqlCommand sqlComm = new SqlCommand(sql, sqlConnection);

                sqlComm.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlConnection.Open();
                var dr = sqlComm.ExecuteReader();
             
                if (dr.Read())
                {
                    personId = dr["PersonId"].ToString();
                    sqlConnection.Close();
                    using (SqlCommand sqlCommand = new SqlCommand("UPDATE Person SET LastName = @LastName, FirstName = @FirstName, BirthDate = @BirthDate WHERE PersonId = @PersonId"))
                    {
                        sqlCommand.Parameters.AddWithValue("@PersonId", personId);
                        sqlCommand.Parameters.AddWithValue("@LastName", lastName);
                        sqlCommand.Parameters.AddWithValue("@FirstName", firstName);
                        sqlCommand.Parameters.AddWithValue("@BirthDate", birthDate);
                        sqlCommand.Connection = sqlConnection;
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                        sqlConnection.Close();
                    }
                }

                using (SqlCommand sqlCommand = new SqlCommand("UPDATE Employee SET EmployeeNum = @EmployeeNum, EmployeeDate = @EmployeeDate, Terminatedate = @Terminatedate WHERE Employee.EmployeeId = @EmployeeId"))
                {
                    sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                    sqlCommand.Parameters.AddWithValue("@EmployeeNum", employeeNum);
                    sqlCommand.Parameters.AddWithValue("@EmployeeDate", employeeDate);
                    sqlCommand.Parameters.AddWithValue("@Terminatedate", terminatedate);
                    sqlCommand.Connection = sqlConnection;
                    sqlConnection.Open();
                    sqlCommand.ExecuteNonQuery();
                    sqlConnection.Close();
                }
            }
        }

        [WebMethod]
        public void DeleteEmployee(string employeeId)
        {
            string personId;
            using (SqlConnection sqlConnection = new SqlConnection(ConnectionState()))
            {
                string sql = "SELECT PersonId FROM Employee WHERE Employee.EmployeeId = @EmployeeId";
                SqlCommand sqlComm = new SqlCommand(sql, sqlConnection);

                sqlComm.Parameters.AddWithValue("@EmployeeId", employeeId);
                sqlConnection.Open();
                var dr = sqlComm.ExecuteReader();

                if (dr.Read())
                {
                    personId = dr["PersonId"].ToString();
                    sqlConnection.Close();

                    using (SqlCommand sqlCommand = new SqlCommand("DELETE FROM Employee WHERE EmployeeId = @EmployeeId"))
                    {
                        sqlCommand.Parameters.AddWithValue("@EmployeeId", employeeId);
                        sqlCommand.Connection = sqlConnection;
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                        sqlConnection.Close();
                    }

                    using (SqlCommand sqlCommand = new SqlCommand("DELETE FROM Person WHERE PersonId = @PersonId"))
                    {
                        sqlCommand.Parameters.AddWithValue("@PersonId", personId);
                        sqlCommand.Connection = sqlConnection;
                        sqlConnection.Open();
                        sqlCommand.ExecuteNonQuery();
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}
